# CheatMeNot16
[TOC]

## Introduction

Avoid cheaters/coin farming bots in FIFA16 Ultimate Team, this tool automatically opens your browser on your opponent's FUT profile to allow you to quickly inspect it. 
Any kind of feedback is appreciated, if you find my tool useful and you feel like showing me your appreciation [I'd gladly accept a beer from you. Food and games are fine too, especially those without cheaters](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=GPJYRAUTA2FZW&lc=GB&item_name=CheatMeNot16&item_number=CHMN16&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted). :)

## How it works
* Video demonstration: https://youtu.be/grOaDe84y2s

Run the main exe or the "--no-browser" shortcut if you don't want it to automatically open your opponent's profile for some reason.
You can run the tool before opening FIFA16 or when you have already reached the FUT main menu (probably the best way to do it), it should work anyway as in the video.

You might need this https://www.microsoft.com/en-gb/download/details.aspx?id=48145 in case you can't run it due to missing DLLs.

## Releases
* BETA5 (22/07/2016) -- Using a different memory location as the ones in BETA4 did not work for some people.
* BETA4 (23/07/2016) -- FIFA16 was patched again to fix chemistry, previous versions will not work anymore.
* BETA3 (20/05/2016) -- FIFA16 was patched, previous versions will not work anymore.
* BETA2 (05/04/2016)
* BETA1 (27/04/2016)

## Cheaters traits
* extremely high winrate. 
* extremely low pass accuracy.
* too many matches for the estabilished date. 

## Why does this tool have way less features than CheatMeNot14?

FUT profiles have all it needs to identify cheaters, checking them is actually the most reliable way to do it. Also I now have way less free time so this is all I can do for now.

# Cheating in FIFA games on PC

## Why

FIFA Ultimate Team is a game mode centered ~on building your team~ acquiring ingame currency. No one likes grinding so a lot of people buy that currency for real money from people who abuse the game and have plenty to sell.
This is a self feeding system that creates inflation and makes people buy even more currency to cope with higher prices.

## How

The same flaw/vulnerability is present in all FIFA games and is easily exploitable on PC: FIFA games are peer-to-peer, Ea servers can't know for sure what's the result of a multiplayer game so cheaters can simply forge fake match reports that grant them wins and currency. 
This process is automated and repeated hundreds of times per day on hundreds different accounts, making the top most-lucrative areas of the game literally unplayable.

## Why isn't EA reacting?

Games are unplayable just for the top 10% of the already small PC userbase, the remaining 90% is only affected by the inflation which has been put under control by enforced prices ranges. 
FIFA games are designed around and for consoles, the PC market share is too small to make EA act especially since everyone is still buying the game anyway.
While there are potential easy fixes that could be implemented (like cross matching game results to flag potential cheaters, captchas to guard the matchmaking queue) fixing this issue for good would require a complete overhaul of the multiplayer and networking logics. You can't really fight cheaters with the current design. **TL;DR It's expensive**.

# Final notes

## Can your tool be used for cheating?
Losers can cherry pick opponents with it. 

## Feedback and contacts
* thatfloppy (at) gmail (dot) com
* https://fifaforums.easports.com/en/discussion/155054/cheatmenot16-a-tool-to-avoid-cheaters/p1
* https://bitbucket.org/dontcopythatfloppy/cheatmenot14-beta
* http://www.reddit.com/r/FIFA/comments/25e80s/fifa14_pc_ultimate_team_cheaters_a_tool_to_avoid/
* http://www.reddit.com/r/Games/comments/25dd5h/due_to_eas_unwillingness_to_deal_with_the/
* [buy me a beer, food or games without cheaters](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=GPJYRAUTA2FZW&lc=GB&item_name=CheatMeNot16&item_number=CHMN16&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted) :)

## Disclaimer 
It's very unlikely that you will get banned for using this anti-cheater tool (oh the irony), yet I can't guarantee anything, EA's position on my tools is: **downloading is at your own risk and it's not endorsed by EA**.

![EA forum post](http://i.imgur.com/tqTJjFQ.jpg)
http://forum.ea.com/uk/posts/list/990/2577535.page
 
Have fun!

Also a huge "thank you" to eur0pa/infausto, the author of the [Dark Souls PVP Watchdog](https://bitbucket.org/infausto/dswatchdog-beta), [he taught me everything i needed to make my previous tool](http://www.reddit.com/r/Games/comments/25dd5h/due_to_eas_unwillingness_to_deal_with_the/chg5aum).